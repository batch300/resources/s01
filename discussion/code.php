<?php 

// It's a rule that we cannot put php codes without s php tag
// we don't have to nessarily add the closing tag 

//[1] Comments
// comments are parts of the code that gets ignored by the language
// comments are meant to describe the written code
/*
	Two types of comments
	- the single-line comment denoted by two slashes
	-the multi-line comment denoted by a slash and asterisk
*/
//[2] Varibales
// Variables are used to conatain data

// Varibales
	$name = "John Smith";
	$developer = "Peter Parker";
	$email = "peterparker@gmail.com";

// Constants
define('PI', 3.1416);
define('HERO','Spiderman');

$HERO = "Jose Rizal";

//HERO = "This will not work";
//Data Types

//Strings
$country ="Philippines";
$city = "Quezon City";

$address = $city . ',' .$country; // This is concatinate
$address = "$city. $country";

//Integers
$age = 31;
$headcount = 26;

// floats
$grade = 98.2;
$distanceFromHome = 101.5;

// Boolean
$isGraduating = true;
$isFailed = false;

//Null
$spouse = null;
$middleName = null;

//Arrays
$grades = array(98.7,92.1,90.2,94.6);
$pokemon = ["Bulbausar", "Charmander","Squirtle"];

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.1,
	'fourthGrading' => 94.6
];

//Mini Activity
//PersonObj
/*function Person() {
	$fullName = "Joram Ape";
	$isGraduating = true;
	$age = 30;
	$address = (object) [
		'province/city' => "Bacolod City Negros Occidental",
		 'country' => "Philippines"

	];
}
*/

$personObj = (object)[
'fullName' => "Joram Ape",
'isGraduating' => true,
'age' => 30,
'address' => (object)[
	'city' => "Manila",
	'country' => "Philippines"
	]
];

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

//[3] Function

function getFullName($firstName, $middleName, $lastName) {

	return "$lastName, $firstName $middleName";
}

// [4] Selection Control Structures
	//If-Elseif-Else statement

function determineTyphooneIntensity($windSpeed) {

	if($windSpeed < 30) {
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61){
		return 'Trophical Storm Detected';
	} else if ($windSpeed >= 62 && $windSpeed <=88) {
		return 'Tropical Storm Detected';

	} else if ($windSpeed >= 89 && $windSpeed <=117) {
		return 'Severe Tropical Storm Detected';
	} 
	else {
		return 'typhoon Detected';
	}
}


function isUnderAge ($age) {
	return ($age < 18) ? true : false;
}

// Switch Fucntion
function determineComputerUser($computerNumber) {
	switch ($computerNumber) {
		case 1:
			return 'Naruto';
			break;
		case 2:
			return 'Sasuke';
			break;
		case 3:
			return 'Sakura';
			break;
		default:
			return $computerNumber. ' is out of bounds.';
			break;
	}
}

function greeting($str) {

	try{

		if (gettype($str) == "string") {
			echo $str;
		} else {
			throw new Exception("Oops! ");
		}
	}

	catch (Exception $error){
		echo $error->getMessage();
	}

	finally{
		 echo "I did it again!";
	}
}