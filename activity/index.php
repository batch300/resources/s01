<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity</title>
</head>
<body>

	<h1>Full Address</h1>
	<p><?= getFullAddress('3F Caswynn Bldg.','Quezon City', 'Metro Manila', 'Philippines'); ?></p>

	<h1>Letter-Based Grading</h1>
	<p><?= getLetterGrade(86); ?></p>
	<p><?= getLetterGrade(97); ?></p>
	<p><?= getLetterGrade(73); ?></p>

</body>
</html>
